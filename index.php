<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S01 Activity</title>
</head>
<body>
	<h1>Letter-Based Grading</h1>
	<p><?php echo getLetterGrade(100) ?></p>
	<p><?php echo getLetterGrade(97) ?></p>
	<p><?php echo getLetterGrade(94) ?></p>
	<p><?php echo getLetterGrade(91) ?></p>
	<p><?php echo getLetterGrade(88) ?></p>
	<p><?php echo getLetterGrade(85) ?></p>
	<p><?php echo getLetterGrade(82) ?></p>
	<p><?php echo getLetterGrade(79) ?></p>
	<p><?php echo getLetterGrade(76) ?></p>
	<p><?php echo getLetterGrade(74) ?></p>
</body>
</html>